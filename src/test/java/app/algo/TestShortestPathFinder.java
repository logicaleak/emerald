package app.algo;

import app.CmNode;
import app.MapReader;
import app.graph.DirectedGraph;
import app.worldofoz.EmeraldCityRoad;
import app.worldofoz.RoadStop;
import org.junit.Assert;
import org.junit.Test;
/**
 * Created by ozum on 6/18/2016.
 */
public class TestShortestPathFinder {
    @Test
    public void Should_Calculate_Correct_Shortest_Path_With_Normalization() throws Exception {
        EmeraldCityRoad emeraldCityRoad = EmeraldCityRoad.builder(8).dice(1, 3)
                .normalized(true)
                .build();
        ShortestPathFinder<EmeraldCityRoad> shortestPathFinder =
                new ShortestPathFinder<>(emeraldCityRoad, new RoadStop(1));
        ShortestPathFinder.ShortestPath shortestPath = shortestPathFinder.calculateShortestPath(new RoadStop(8));

        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(1), 0), shortestPath.path.get(0));
        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(4), 1), shortestPath.path.get(1));
        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(7), 2), shortestPath.path.get(2));
        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(8), 3), shortestPath.path.get(3));
        Assert.assertEquals(Integer.valueOf(3), shortestPath.cost);
    }

    @Test
    public void Should_Calculate_Correct_Shortest_Path_Without_Normalization() throws Exception {
        EmeraldCityRoad emeraldCityRoad =
                EmeraldCityRoad.builder(6).whitePortal(2, 4).dice(1, 2).normalized(false).build();

        ShortestPathFinder<EmeraldCityRoad> shortestPathFinder =
                new ShortestPathFinder<>(emeraldCityRoad, new RoadStop(1));
        ShortestPathFinder.ShortestPath shortestPath = shortestPathFinder.calculateShortestPath(new RoadStop(6));

        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(1), 0), shortestPath.path.get(0));
        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(4), 1), shortestPath.path.get(1));
        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(5), 2), shortestPath.path.get(2));
        Assert.assertEquals(new ShortestPathFinder.ArrivalNode(new RoadStop(6), 3), shortestPath.path.get(3));
        Assert.assertEquals(Integer.valueOf(3), shortestPath.cost);
    }

    @Test
    public void Bla() throws Exception {
        EmeraldCityRoad emeraldCityRoad = EmeraldCityRoad.builder(17).dice(1, 3)
                .normalized(true)
                .build();
        ShortestPathFinder<EmeraldCityRoad> shortestPathFinder =
                new ShortestPathFinder<>(emeraldCityRoad, new RoadStop(1));
        ShortestPathFinder.ShortestPath shortestPath = shortestPathFinder.calculateShortestPath(new RoadStop(17));
    }

    @Test
    public void Bla2() throws Exception {
        EmeraldCityRoad emeraldCityRoad = EmeraldCityRoad.builder(17).dice(1, 3).whitePortal(2, 17)
                .normalized(true)
                .build();

        PriorityShortestPathFinder shortestPathFinder = new PriorityShortestPathFinder<>(emeraldCityRoad);
        shortestPathFinder.calculateShortestPath(new RoadStop(1), new RoadStop(17));

    }

    @Test
    public void Blaaaaa() throws Exception {
        DirectedGraph map = MapReader.startRead("/data.dat").read().getGraph();
        PriorityShortestPathFinder priorityShortestPathFinder = new PriorityShortestPathFinder(map);
        PriorityShortestPathFinder.ShortestPath shortestPath =
                priorityShortestPathFinder.calculateShortestPath(new CmNode("314179217"), new CmNode("314179260"));
    }
}
