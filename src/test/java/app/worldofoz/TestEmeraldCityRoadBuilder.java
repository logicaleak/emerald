package app.worldofoz;

import app.worldofoz.exception.PortalEligibilityException;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;

/**
 * Created by ozum on 6/18/2016.
 */
public class TestEmeraldCityRoadBuilder {
    @Test
    public void Should_Build_Properly_Without_Black_Portal() throws Exception {
        EmeraldCityRoad emeraldCityRoad =
                EmeraldCityRoad.builder(6).whitePortal(2, 6).dice(1, 2).build();

        List<Road> connections1 = emeraldCityRoad.getRoadList(new RoadStop(1));
        List<Road> connections2 = emeraldCityRoad.getRoadList(new RoadStop(2));
        List<Road> connections3 = emeraldCityRoad.getRoadList(new RoadStop(3));
        List<Road> connections4 = emeraldCityRoad.getRoadList(new RoadStop(4));
        List<Road> connections5 = emeraldCityRoad.getRoadList(new RoadStop(5));
        List<Road> connections6 = emeraldCityRoad.getRoadList(new RoadStop(6));

        Road c;
        /**
         * Check connections1
         */
        Assert.assertEquals(2, connections1.size());
        c = connections1.get(0);
        Assert.assertEquals(new RoadStop(6), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);
        c = connections1.get(1);
        Assert.assertEquals(new RoadStop(3), c.to);

        /**
         * Check connections2
         */
        Assert.assertEquals(0, connections2.size());

        /**
         * Check connections3
         */
        Assert.assertEquals(2, connections3.size());
        c = connections3.get(0);
        Assert.assertEquals(new RoadStop(4), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);
        c = connections3.get(1);
        Assert.assertEquals(new RoadStop(5), c.to);
        Assert.assertEquals(Integer.valueOf(2), c.cost);

        /**
         * Check connections4
         */
        Assert.assertEquals(2, connections4.size());
        c = connections4.get(0);
        Assert.assertEquals(new RoadStop(5), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);
        c = connections4.get(1);
        Assert.assertEquals(new RoadStop(6), c.to);
        Assert.assertEquals(Integer.valueOf(2), c.cost);

        /**
         * Check connections5
         */
        Assert.assertEquals(1, connections5.size());
        c = connections5.get(0);
        Assert.assertEquals(new RoadStop(6), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);

        /**
         * Check connections6
         */
        Assert.assertEquals(0, connections6.size());
    }

    @Test
    public void Should_Properly_Build_With_Black_And_White_Portals() throws Exception {
        EmeraldCityRoad emeraldCityRoad = EmeraldCityRoad.builder(8).dice(1, 3)
                .blackPortal(5, 3)
                .whitePortal(2, 4)
                .whitePortal(7, 8)
                .blackPortal(6, 2)
                .build();

        List<Road> connections1 = emeraldCityRoad.getRoadList(new RoadStop(1));
        List<Road> connections2 = emeraldCityRoad.getRoadList(new RoadStop(2));
        List<Road> connections3 = emeraldCityRoad.getRoadList(new RoadStop(3));
        List<Road> connections4 = emeraldCityRoad.getRoadList(new RoadStop(4));
        List<Road> connections5 = emeraldCityRoad.getRoadList(new RoadStop(5));
        List<Road> connections6 = emeraldCityRoad.getRoadList(new RoadStop(6));
        List<Road> connections7 = emeraldCityRoad.getRoadList(new RoadStop(7));
        List<Road> connections8 = emeraldCityRoad.getRoadList(new RoadStop(8));


        Road c;
        /**
         * Check connections1
         */
        Assert.assertEquals(3, connections1.size());
        c = connections1.get(0);
        Assert.assertEquals(new RoadStop(4), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);
        c = connections1.get(1);
        Assert.assertEquals(new RoadStop(3), c.to);
        Assert.assertEquals(Integer.valueOf(2), c.cost);
        c = connections1.get(2);
        Assert.assertEquals(new RoadStop(4), c.to);
        Assert.assertEquals(Integer.valueOf(3), c.cost);

        /**
         * Check connections2
         */
        Assert.assertEquals(0, connections2.size());

        /**
         * Check connections3
         */
        Assert.assertEquals(3, connections3.size());
        c = connections3.get(0);
        Assert.assertEquals(new RoadStop(4), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);
        c = connections3.get(1);
        Assert.assertEquals(new RoadStop(3), c.to);
        Assert.assertEquals(Integer.valueOf(2), c.cost);
        c = connections3.get(2);
        Assert.assertEquals(new RoadStop(4), c.to);
        Assert.assertEquals(Integer.valueOf(3), c.cost);

        /**
         * Check connections4
         */
        Assert.assertEquals(3, connections4.size());
        c = connections4.get(0);
        Assert.assertEquals(new RoadStop(3), c.to);
        Assert.assertEquals(Integer.valueOf(1), c.cost);
        c = connections4.get(1);
        Assert.assertEquals(new RoadStop(4), c.to);
        Assert.assertEquals(Integer.valueOf(2), c.cost);
        c = connections4.get(2);
        Assert.assertEquals(new RoadStop(8), c.to);
        Assert.assertEquals(Integer.valueOf(3), c.cost);

        /**
         * Check connections5
         */
        Assert.assertEquals(0, connections5.size());

        /**
         * Check connections6
         */
        Assert.assertEquals(0, connections6.size());

        /**
         * Check connections7
         */
        Assert.assertEquals(0, connections7.size());

        /**
         * Check connections8
         */
        Assert.assertEquals(0, connections8.size());
    }

    @Test
    public void Should_Properly_Build_When_Cascaded_White_Portals() throws Exception {
        EmeraldCityRoad emeraldCityRoad = EmeraldCityRoad.builder(6).dice(1, 2)
                .whitePortal(2, 4)
                .whitePortal(4, 6)
                .build();

        List<Road> roads1 = emeraldCityRoad.getRoadList(new RoadStop(1));
        List<Road> roads2 = emeraldCityRoad.getRoadList(new RoadStop(2));

        Road r;
        /**
         * Roads1
         */
        Assert.assertEquals(2, roads1.size());
        r = roads1.get(0);
        Assert.assertEquals(new RoadStop(6), r.to);
        Assert.assertEquals(Integer.valueOf(1), r.cost);
        r = roads1.get(1);
        Assert.assertEquals(new RoadStop(3), r.to);
        Assert.assertEquals(Integer.valueOf(2), r.cost);

        /**
         * Roads2
         */
        Assert.assertEquals(0, roads2.size());
    }

    @Test(expected = PortalEligibilityException.class)
    public void Should_Throw_Exception_For_Clashing_BlackWhiteDepart_Portals() throws Exception {
        EmeraldCityRoad.builder(6).whitePortal(2, 6).blackPortal(2, 1).dice(1, 2).build();
    }

    @Test(expected = PortalEligibilityException.class)
    public void Should_Throw_Exception_For_Clashing_WhiteWhiteDepart_Portals() throws Exception {
        EmeraldCityRoad.builder(6).whitePortal(2, 6).whitePortal(2, 1).dice(1, 2).build();
    }

    @Test(expected = PortalEligibilityException.class)
    public void Should_Throw_Exception_For_Clashing_BlackBlackDepart_Portals() throws Exception {
        EmeraldCityRoad.builder(6).blackPortal(6, 2).blackPortal(6, 3).dice(1, 2).build();
    }

    @Test(expected = PortalEligibilityException.class)
    public void Should_Throw_Exception_For_Clashing_BlackWhiteArrival_Portals() throws Exception {
        EmeraldCityRoad.builder(6).whitePortal(2, 3).blackPortal(5, 3).dice(1, 2).build();
    }

    @Test(expected = PortalEligibilityException.class)
    public void Should_Throw_Exception_For_Clashing_WhiteWhiteArrival_Portals() throws Exception {
        EmeraldCityRoad.builder(6).whitePortal(2, 3).whitePortal(1, 3).dice(1, 2).build();
    }

    @Test(expected = PortalEligibilityException.class)
    public void Should_Throw_Exception_For_Clashing_BlackBlackArrival_Portals() throws Exception {
        EmeraldCityRoad.builder(6).blackPortal(6, 3).blackPortal(5, 3).dice(1, 2).build();
    }

}
