package app.algo;

import java.util.ArrayList;

/**
 * Created by ozum on 6/19/2016.
 */
public class ImmutableArrayList <T> extends ArrayList<T> {
    @Override
    public T set(int index, T element) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }
}
