package app.algo.exception;

/**
 * Created by ozum on 20.06.2016.
 */
public class IllegalDestinationException extends Exception {
    public IllegalDestinationException(String message) {
        super(message);
    }
}
