package app.algo;

import app.graph.DirectedGraph;
import java.util.*;

/**
 * Created by ozum on 6/18/2016.
 */
public class ShortestPathFinder<G extends DirectedGraph> {
    /**
     * The app.graph on which shortest path algorithm will be ran
     */
    private final G graph;
    private final DirectedGraph.Node startNode;
    /**
     * Nodes with their final smallest reach costs resides in this map
     */
    private Map<DirectedGraph.Node, Best> completed;
    /**
     * Nodes with uncertain smallest reach costs resides in this map
     */
    private Map<DirectedGraph.Node, Best> onGoing;


    private static class Best {
        public final Integer cost;
        public final DirectedGraph.Node from;

        public Best(Integer cost, DirectedGraph.Node from) {
            this.cost = cost;
            this.from = from;
        }
    }

    //todo Fix the equals/hashcode contract
    //todo DON'T USE IT IN A HASH STRUCTURE
    /**
     *
     */
    public static class ArrivalNode {
        public final DirectedGraph.Node arrivalTo;
        public final Integer arrivalCost;

        public ArrivalNode(DirectedGraph.Node arrivalTo, Integer arrivalCost) {
            this.arrivalTo = arrivalTo;
            this.arrivalCost = arrivalCost;
        }

        @Override
        public String toString() {
            return arrivalTo.toString() + " , cost : " + arrivalCost.toString();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof ArrivalNode)) {
                return false;
            }
            ArrivalNode other = (ArrivalNode) obj;
            return this.arrivalTo.equals(other.arrivalTo) &&
                    this.arrivalCost.equals(other.arrivalCost);
        }
    }

    public static class ShortestPath {
        public final Integer cost;
        public final List<ArrivalNode> path;

        public ShortestPath(Integer cost, List<ArrivalNode> path) {
            this.cost = cost;
            this.path = path;
        }
    }

    public ShortestPathFinder(G graph, DirectedGraph.Node startNode) {
        this.graph = graph;
        this.startNode = startNode;
        this.completed = new HashMap<>();
        this.onGoing = new HashMap<>();
    }

    public ShortestPath calculateShortestPath(DirectedGraph.Node dest) {
        //We can put the start node into the completed map
        onGoing.put(startNode, new Best(0, startNode));

        //Here the wildcard will take something that extends DirectedGraph.Edge
        DirectedGraph.Node currentNode = startNode;
        DirectedGraph.Node previousNode = startNode;
        while (true) {
            Best onGoingBestOfCurrentNode = onGoing.get(currentNode);
            completed.put(currentNode, new Best(onGoingBestOfCurrentNode.cost, previousNode));//Complete the next
            onGoing.remove(currentNode);

            List<DirectedGraph.Edge> edgesOfCurrentNode = (List<DirectedGraph.Edge>)(List<?>) graph.getEdges(currentNode);
            //Because we have arrived to current node in previous loop, we know its cost can be find in completed map
            Integer costOfShortestPathToCurrentNode = completed.get(currentNode).cost;

            //We have arrived to the destination node, so we shall stop the loop
            if (completed.get(dest) != null) {
                break;
            }

            //The nodes other than the closest are set into ongoing
            DirectedGraph.Node nextNode = null;
            //Set smallestCost to first of the list
            int smallestCost = edgesOfCurrentNode.get(0).cost + costOfShortestPathToCurrentNode;
            for (DirectedGraph.Edge edge : edgesOfCurrentNode) {
                //Check if the destination of the edge is already completed
                Best finalCostToNode = completed.get(edge.to);
                if (finalCostToNode != null || edge.to.equals(currentNode)) {
                    continue;
                }

                //If currentCost to reach this node is bigger than the new cost update it
                //If there is no current cost, set the new cost to it
                Integer newCostForTheNode = edge.cost + costOfShortestPathToCurrentNode;
                Integer bestCostForTheNode = edge.cost + costOfShortestPathToCurrentNode;
                Best currentCostOfTheNode = onGoing.get(edge.to);
                if (currentCostOfTheNode != null) {
                    if (newCostForTheNode < currentCostOfTheNode.cost) {
                        onGoing.put(edge.to, new Best(newCostForTheNode, currentNode));
                    } else {
                        bestCostForTheNode = currentCostOfTheNode.cost;
                    }
                } else {
                    onGoing.put(edge.to, new Best(newCostForTheNode, currentNode));
                }

                if (bestCostForTheNode <= smallestCost) {
                    smallestCost = bestCostForTheNode;
                    nextNode = edge.to;
                }
            }

            List<DirectedGraph.Edge> edgesOfNextNode = (List<DirectedGraph.Edge>)(List<?>) graph.getEdges(nextNode);
            //There is no where to go
            if (((edgesOfNextNode != null && edgesOfNextNode.size() == 0) || nextNode == null) && !nextNode.equals(dest)) {
                Integer smallestOngoingCost = 0;
                Boolean first = true;
                for (Map.Entry entry : onGoing.entrySet()) {
                    DirectedGraph.Node nodeTo = (DirectedGraph.Node) entry.getKey();
                    edgesOfNextNode = (List<DirectedGraph.Edge>)(List<?>) graph.getEdges(nodeTo);
                    Best onGoingBest = (Best) entry.getValue();
                    if (first) {
                        smallestOngoingCost = onGoingBest.cost;
                        first = false;
                        nextNode = nodeTo;
                    } else {
                        if (onGoingBest.cost <= smallestOngoingCost && edgesOfNextNode.size() != 0) {
                            smallestOngoingCost = onGoingBest.cost;
                            nextNode = nodeTo;
                        }
                    }

                }

                previousNode = onGoing.get(nextNode).from;
            } else {
                previousNode = currentNode;
            }
            currentNode = nextNode;
        }

        Best finalBestForDest = completed.get(dest);
        List<ArrivalNode> l = new ArrayList<>();
        l.add(new ArrivalNode(dest, finalBestForDest.cost));
        Integer previousCost = finalBestForDest.cost;
        Integer cost = 0;
        while (true) {
            DirectedGraph.Node lastFrom = finalBestForDest.from;

            finalBestForDest = completed.get(finalBestForDest.from);
            Integer lastFromCost = finalBestForDest.cost;
            l.add(new ArrivalNode(lastFrom, lastFromCost));
            cost += previousCost - lastFromCost;

            previousCost = lastFromCost;


            if (lastFrom.equals(startNode)) {
                break;
            }
        }

        //todo
//        Collections.reverse(l);
        List<ArrivalNode> unmodifiablePath = Collections.unmodifiableList(l);

        return new ShortestPath(cost, new ImmutableReversedArrayList<>(l));
    }
}
