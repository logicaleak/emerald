package app.worldofoz;

import app.worldofoz.exception.PortalEligibilityException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ozum on 6/18/2016.
 */

/**
 * Builds a Emerald city road graph from the given parameters
 */
public class EmeraldCityRoadBuilder {
    private Boolean normalized;
    private int stopNumber;
    private int diceLowerBound;
    private int diceUpperBound;
    private Map<RoadStop, RoadStop> whitePortals;
    private Map<RoadStop, RoadStop> whitePortalsInverted;
    private Map<RoadStop, RoadStop> blackPortals;
    private Map<RoadStop, RoadStop> blackPortalsInverted;


    public EmeraldCityRoadBuilder(int stopNumber) {
        this.stopNumber = stopNumber;
        this.whitePortals = new HashMap<>();
        this.blackPortals = new HashMap<>();
        this.whitePortalsInverted = new HashMap<>();
        this.blackPortalsInverted = new HashMap<>();
        this.normalized = false;
    }

    //todo Simplify
    private Boolean checkWhitePortalDepartEligibility(int from) {
        return whitePortals.get(new RoadStop(from)) == null;
    }

    private Boolean checkBlackPortalDepartEligibility(int from) {
        return blackPortals.get(new RoadStop(from)) == null;
    }

    private Boolean checkWhitePortalArrivalEligibility(int to) {
        return whitePortalsInverted.get(new RoadStop(to)) == null;
    }

    private Boolean checkBlackPortalArrivalEligibility(int to) {
        return blackPortalsInverted.get(new RoadStop(to)) == null;
    }

    private void checkForEligibility(int from, int to) throws PortalEligibilityException {
        if (!checkWhitePortalArrivalEligibility(to) || !checkBlackPortalArrivalEligibility(to)) {
            throw new PortalEligibilityException("There is already a portal to this destination");
        }

        if (!checkWhitePortalDepartEligibility(from) || !checkBlackPortalDepartEligibility(from)) {
            throw new PortalEligibilityException("There is already a portal from this road stop");
        }
    }

    /**
     * Add a white portal
     * @param from the stop from which the portal will transfer
     * @param to the stop to which the portal will transfer
     * @return this
     * @throws PortalEligibilityException If duplicate portals or non-progressive white portal
     */
    public EmeraldCityRoadBuilder whitePortal(int from, int to) throws PortalEligibilityException {
        if (to <= from) {
            throw new PortalEligibilityException("White portal destination must be progressive");
        }
        checkForEligibility(from, to); //throws exception if not eligible
        this.whitePortals.put(new RoadStop(from), new RoadStop(to));
        this.whitePortalsInverted.put(new RoadStop(to), new RoadStop(from));
        return this;
    }

    /**
     * Add a black portal
     * @param from the stop from which the portal will transfer
     * @param to the stop to which the portal will transfer
     * @return this
     * @throws PortalEligibilityException If duplicate portals or progressive black portal
     */
    public EmeraldCityRoadBuilder blackPortal(int from, int to) throws PortalEligibilityException {
        if (from <= to) {
            throw new PortalEligibilityException("Destination cannot be progressive for a black portal");
        }
        checkForEligibility(from, to); //throws exception if not eligible
        this.blackPortals.put(new RoadStop(from), new RoadStop(to));
        this.blackPortalsInverted.put(new RoadStop(to), new RoadStop(from));
        return this;
    }

    /**
     * Determine the dice bounds
     * @param lowerBound lowest possible dice result
     * @param upperBound highest possible dice result
     * @return this
     */
    public EmeraldCityRoadBuilder dice(int lowerBound, int upperBound) {
        this.diceLowerBound = lowerBound;
        this.diceUpperBound = upperBound;
        return this;
    }

    /**
     * If EmeraldCityRoad is normalized, roads will have number of dice rolls as cost,
     * assuming the dice always give the highest, instead of having the dice result as cost
     *
     * If this is not set to true, shortest path calculation will vary compared to normal
     * @param isNormalized true if normalized
     * @return this
     */
    public EmeraldCityRoadBuilder normalized(Boolean isNormalized) {
        this.normalized = isNormalized;
        return this;
    }

    /**
     * Iterates until reaches a stop where no portals exist
     * @param candidateDest destination
     * @return final stop
     */
    private RoadStop findFinalDestinationThroughPortal(RoadStop candidateDest) {
        while (true) {
            RoadStop whitePortalDest = this.whitePortals.get(candidateDest);
            if (whitePortalDest != null) {
                candidateDest = whitePortalDest;
                continue;
            }

            RoadStop blackPortalDest = this.blackPortals.get(candidateDest);
            if (blackPortalDest != null) {
                candidateDest = blackPortalDest;
                continue;
            }

            return candidateDest;
        }
    }

    /**
     * Build the structure
     * @return EmeraldCityRoad graph
     */
    public EmeraldCityRoad build() {
        EmeraldCityRoad emeraldCityRoad = new EmeraldCityRoad();

        //Iterate the stops and create roads
        for (int i = 1; i < this.stopNumber + 1; i++) {
            RoadStop currentRoadStop = new RoadStop(i);
            emeraldCityRoad.addRoadStop(currentRoadStop);

            //If there is a white portal from this, there should not be any connections
            RoadStop whitePortalDest = this.whitePortals.get(currentRoadStop);
            if (whitePortalDest != null) {
                continue;
            }

            //If there is a black portal from this, there should not be any connections
            RoadStop blackPortalDest = this.blackPortals.get(currentRoadStop);
            if (blackPortalDest != null) {
                continue;
            }

            //Iterate between dice bounds to determine reachable stops from the stop {i}
            for (int dice = this.diceLowerBound; dice < this.diceUpperBound + 1; dice++) {
                if (this.stopNumber < i + dice) {
                    continue;
                }

                RoadStop destinationStop = new RoadStop(i + dice);

                Integer cost = dice;
                //If a normalized app.graph is requested, every cost will be one
                if (this.normalized) {
                    cost = 1;
                }

                //Check if there are portals at the destination
                //If there are portals, the real destination will depend on where the portals connect
                whitePortalDest = this.whitePortals.get(destinationStop);
                blackPortalDest = this.blackPortals.get(destinationStop);
                Road road;
                if (whitePortalDest != null) {
                    RoadStop finalDest = findFinalDestinationThroughPortal(whitePortalDest);
                    road = new Road(finalDest, cost);
                } else if (blackPortalDest != null) {
                    RoadStop finalDest = findFinalDestinationThroughPortal(blackPortalDest);
                    road = new Road(finalDest, cost);
                } else {
                    //There are no portals
                    road = new Road(destinationStop, cost);
                }
                emeraldCityRoad.addRoad(currentRoadStop, road);
            }
        }

        emeraldCityRoad.stopNumber = this.stopNumber;
        return emeraldCityRoad;
    }


}
