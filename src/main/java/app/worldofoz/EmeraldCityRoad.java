package app.worldofoz;

import app.algo.ShortestPathFinder;
import app.algo.exception.IllegalDestinationException;
import app.graph.DirectedGraph;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ozum on 6/18/2016.
 */

/**
 * Data structure that represents Emerald City Road
 */
public class EmeraldCityRoad extends DirectedGraph<RoadStop, Road> {
    protected Integer stopNumber;

    /**
     * Instantiates a builder
     * @param stopNumber Number of stops
     * @return the builder
     */
    public static EmeraldCityRoadBuilder builder(int stopNumber) {
        EmeraldCityRoadBuilder emeraldCityRoadBuilder =
                new EmeraldCityRoadBuilder(stopNumber);
        return emeraldCityRoadBuilder;
    }

    /**
     * Returns the list of the roads from the input node
     * Does not return the list itself but rather copies it
     * This method basically does the same with getEdges method
     * But created for convenience of the meaning it has
     *
     * @param fromNode Node of which the roads will be returned
     * @return list of roads
     */
    public List<Road> getRoadList(RoadStop fromNode) {
        return Collections.unmodifiableList(this.edgeListMap.get(fromNode));
    }

    /**
     * Provide mutability for builder within the same package, or for
     * the classes to extend it
     *
     * @param roadStop roadStop to add
     */
    protected void addRoadStop(RoadStop roadStop) {
        super.addNode(roadStop);
    }

    /**
     * Provide mutability for builder within the same package, or
     * for the classes to extend it
     *
     * @param fromNode the node to which a road will be added
     * @param road     Road to add
     */
    protected void addRoad(RoadStop fromNode, Road road) {
        super.addEdge(fromNode, road);
    }

    /**
     * Override the base method to prevent emeraldCityRoad from exposing
     * its own edge list, but rather expose a copy
     *
     * @param node Node from which roads will be returned
     * @return List of roads
     */
    @Override
    public List<Road> getEdges(RoadStop node) {
        return Collections.unmodifiableList(super.getEdges(node));
    }

    /**
     * Provides immutability
     *
     * @param node Node to add
     */
    @Override
    public void addNode(RoadStop node) {
        throw new UnsupportedOperationException("Not Supported");
    }

    /**
     * Provides immutability
     *
     * @param fromNode the stop to which the roads will connect
     * @param edge the road
     */
    @Override
    public void addEdge(RoadStop fromNode, Road edge) {
        throw new UnsupportedOperationException("Not Supported");
    }

    /**
     * Gives the first stop of the roads
     * @return first stop
     */
    public RoadStop getFirstStop() {
        return new RoadStop(1);
    }

    /**
     * Gives the last stop of the roads
     * @return last stop
     */
    public RoadStop getLastStop() {
        return new RoadStop(this.stopNumber);
    }

    /**
     * Inner class to represent the shortest path
     * to a destination
     */
    public static class ShortestEmeraldRoad {
        public final Integer cost;
        public final List<RoadStop> path;

        public ShortestEmeraldRoad(Integer cost, List<RoadStop> path) {
            this.cost = cost;
            this.path = path;
        }
    }

    /**
     * Calculates the shortest distance to the destination
     * from the first stop
     * @param destination Destination stop
     * @return Shortest path
     */
    public ShortestEmeraldRoad calculateShortestPath(RoadStop destination) throws IllegalDestinationException {
        if (destination.number > this.getNodeNumber()) {
            throw new IllegalDestinationException("This destination does not exist");
        }

        ShortestPathFinder shortestPathFinder = new ShortestPathFinder<>(this, this.getFirstStop());
        ShortestPathFinder.ShortestPath shortestPath = shortestPathFinder.calculateShortestPath(destination);

        //Cannot use java8 streaming here because ReversedImmutableList does not work that way
        List<RoadStop> path = new ArrayList<>();
        for (ShortestPathFinder.ArrivalNode node : shortestPath.path) {
            path.add(new RoadStop(((RoadStop)node.arrivalTo).number));
        }
        return new ShortestEmeraldRoad(shortestPath.cost, path);
    }

    public Map<RoadStop, List<Road>> getGraphMap() {
        return Collections.unmodifiableMap(this.edgeListMap);
    }
}
