package app.worldofoz;

import app.graph.DirectedGraph;

/**
 * Created by ozum on 6/18/2016.
 */

/**
 * Represents a road stop
 */
public class RoadStop extends DirectedGraph.Node {
    public final Integer number;

    public RoadStop(Integer number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof RoadStop) {
            RoadStop input = (RoadStop) obj;
            return input.number.equals(this.number);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public String toString() {
        return "RoadStopModel{" +
                "number=" + number +
                '}';
    }
}
