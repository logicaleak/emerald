package app.worldofoz.exception;

/**
 * Created by ozum on 6/19/2016.
 */
public class IncompleteParametersException extends Exception {
    public IncompleteParametersException(String message) {
        super(message);
    }
}
