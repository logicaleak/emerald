package app.worldofoz.exception;

/**
 * Created by ozum on 6/18/2016.
 */
public class PortalEligibilityException extends Exception {
    public PortalEligibilityException(String message) {
        super(message);
    }
}
