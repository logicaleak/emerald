package app.worldofoz;

import app.graph.DirectedGraph;

/**
 * Created by ozum on 6/18/2016.
 */

/**
 * Represents a road between two road stops
 */
public class Road extends DirectedGraph.Edge<RoadStop> {
    public Road(RoadStop to, Integer cost) {
        super(to, cost);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
