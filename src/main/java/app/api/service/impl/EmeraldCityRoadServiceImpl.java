package app.api.service.impl;

import app.algo.exception.IllegalDestinationException;
import app.api.model.*;
import app.api.service.EmeraldCityRoadService;
import app.worldofoz.Road;
import org.springframework.stereotype.Component;
import app.worldofoz.EmeraldCityRoad;
import app.worldofoz.EmeraldCityRoadBuilder;
import app.worldofoz.RoadStop;
import app.worldofoz.exception.PortalEligibilityException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by ozum on 6/19/2016.
 */
@Component
public class EmeraldCityRoadServiceImpl implements EmeraldCityRoadService {


    @Override
    public PostShortestPathResponse getShortestPath(PostShortestPathRequest postShortestPathRequest)
            throws PortalEligibilityException, IllegalDestinationException {

        Integer stopNumber = postShortestPathRequest.getStopNumber();
        Integer diceLowerBound = postShortestPathRequest.getDiceLowerBound();
        Integer diceUpperBound = postShortestPathRequest.getDiceUpperBound();
        List<Portal> whitePortals = postShortestPathRequest.getWhitePortals();
        List<Portal> blackPortals = postShortestPathRequest.getBlackPortals();

        EmeraldCityRoadBuilder emeraldCityRoadBuilder = EmeraldCityRoad.builder(stopNumber)
                                                                        .dice(diceLowerBound, diceUpperBound);

        for (Portal portal : whitePortals) {
            emeraldCityRoadBuilder.whitePortal(portal.getFrom(), portal.getTo());
        }

        for (Portal portal : blackPortals) {
            emeraldCityRoadBuilder.blackPortal(portal.getFrom(), portal.getTo());
        }

        EmeraldCityRoad emeraldCityRoad = emeraldCityRoadBuilder.normalized(true).build();

        EmeraldCityRoad.ShortestEmeraldRoad shortestEmeraldRoad;
        if (postShortestPathRequest.getDestination() == null) {
            shortestEmeraldRoad = emeraldCityRoad.calculateShortestPath(emeraldCityRoad.getLastStop());
        } else {
            shortestEmeraldRoad = emeraldCityRoad.calculateShortestPath(new RoadStop(postShortestPathRequest.getDestination()));
        }

        PostShortestPathResponse postShortestPathResponse = new PostShortestPathResponse();

        Path pathForResponse = getPathForResponse(shortestEmeraldRoad);
        postShortestPathResponse.setPath(pathForResponse);

        return postShortestPathResponse;
    }

    private Path getPathForResponse(EmeraldCityRoad.ShortestEmeraldRoad shortestEmeraldRoad) {
        Path shortestPathResponse = new Path();
        shortestPathResponse.setCost(shortestEmeraldRoad.cost);

        List<Integer> thePathInIntegers = shortestEmeraldRoad.path.stream().map(roadStop -> {
            return roadStop.number;
        }).collect(Collectors.toList());
        shortestPathResponse.setPath(thePathInIntegers);
        return shortestPathResponse;
    }

    private RoadStopModel getRoadStopResponseModel(Integer number, List<Road> roadList) {
        List<Integer> roadListResponse = roadList.stream().map(road -> {
            return road.to.number;
        }).collect(Collectors.toList());

        return new RoadStopModel(roadListResponse, number);
    }

    @Override
    public PostEmeraldCityRoadResponse getEmeraldCityRoadDetails(PostEmeraldCityRoadRequest postEmeraldCityRoadRequest)
            throws PortalEligibilityException, IllegalDestinationException {
        Integer stopNumber = postEmeraldCityRoadRequest.getStopNumber();
        Integer diceLowerBound = postEmeraldCityRoadRequest.getDiceLowerBound();
        Integer diceUpperBound = postEmeraldCityRoadRequest.getDiceUpperBound();
        List<Portal> whitePortals = postEmeraldCityRoadRequest.getWhitePortals();
        List<Portal> blackPortals = postEmeraldCityRoadRequest.getBlackPortals();

        EmeraldCityRoadBuilder emeraldCityRoadBuilder = EmeraldCityRoad.builder(stopNumber)
                .dice(diceLowerBound, diceUpperBound);

        for (Portal portal : whitePortals) {
            emeraldCityRoadBuilder.whitePortal(portal.getFrom(), portal.getTo());
        }

        for (Portal portal : blackPortals) {
            emeraldCityRoadBuilder.blackPortal(portal.getFrom(), portal.getTo());
        }

        EmeraldCityRoad emeraldCityRoad = emeraldCityRoadBuilder.normalized(true).build();

        EmeraldCityRoad.ShortestEmeraldRoad shortestEmeraldRoad =
                emeraldCityRoad.calculateShortestPath(emeraldCityRoad.getLastStop());

        Path shortestPathResponse = getPathForResponse(shortestEmeraldRoad);

        PostEmeraldCityRoadResponse postEmeraldCityRoadResponse = new PostEmeraldCityRoadResponse();
        postEmeraldCityRoadResponse.setShortestPath(shortestPathResponse);

        Map<RoadStop, List<Road>> roadMap = emeraldCityRoad.getGraphMap();
        List<RoadStopModel> roadStopModels = new LinkedList<>();
        for (int i = 1; i < stopNumber + 1; i++) {
            List<Road> roadList = roadMap.get(new RoadStop(i));
            RoadStopModel roadStopModel = getRoadStopResponseModel(i, roadList);
            roadStopModels.add(roadStopModel);
        }

        postEmeraldCityRoadResponse.setRoadStops(roadStopModels);
        return postEmeraldCityRoadResponse;
    }
}
