package app.api.service;

import app.algo.exception.IllegalDestinationException;
import app.api.model.PostEmeraldCityRoadRequest;
import app.api.model.PostEmeraldCityRoadResponse;
import app.api.model.PostShortestPathRequest;
import app.api.model.PostShortestPathResponse;
import app.worldofoz.exception.PortalEligibilityException;

/**
 * Created by ozum on 6/19/2016.
 */
public interface EmeraldCityRoadService {
    PostShortestPathResponse getShortestPath(PostShortestPathRequest postShortestPathRequest) throws PortalEligibilityException, IllegalDestinationException;
    PostEmeraldCityRoadResponse getEmeraldCityRoadDetails(PostEmeraldCityRoadRequest postEmeraldCityRoadRequest) throws PortalEligibilityException, IllegalDestinationException;
}
