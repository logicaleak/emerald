package app.api.model;

import java.util.List;

/**
 * Created by ozum on 6/19/2016.
 */
public class PostEmeraldCityRoadRequest {
    private Integer stopNumber;
    private Integer diceLowerBound;
    private Integer diceUpperBound;
    private List<Portal> whitePortals;
    private List<Portal> blackPortals;

    public Integer getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(Integer stopNumber) {
        this.stopNumber = stopNumber;
    }

    public Integer getDiceLowerBound() {
        return diceLowerBound;
    }

    public void setDiceLowerBound(Integer diceLowerBound) {
        this.diceLowerBound = diceLowerBound;
    }

    public Integer getDiceUpperBound() {
        return diceUpperBound;
    }

    public void setDiceUpperBound(Integer diceUpperBound) {
        this.diceUpperBound = diceUpperBound;
    }

    public List<Portal> getWhitePortals() {
        return whitePortals;
    }

    public void setWhitePortals(List<Portal> whitePortals) {
        this.whitePortals = whitePortals;
    }

    public List<Portal> getBlackPortals() {
        return blackPortals;
    }

    public void setBlackPortals(List<Portal> blackPortals) {
        this.blackPortals = blackPortals;
    }
}
