package app.api.model;

import java.util.List;

/**
 * Created by ozum on 6/19/2016.
 */
public class PostEmeraldCityRoadResponse {
    private List<RoadStopModel> roadStops;
    private Path shortestPath;

    public List<RoadStopModel> getRoadStops() {
        return roadStops;
    }

    public void setRoadStops(List<RoadStopModel> roadStops) {
        this.roadStops = roadStops;
    }

    public Path getShortestPath() {
        return shortestPath;
    }

    public void setShortestPath(Path shortestPath) {
        this.shortestPath = shortestPath;
    }
}
