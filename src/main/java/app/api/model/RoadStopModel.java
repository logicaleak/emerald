package app.api.model;

import java.util.List;

/**
 * Created by ozum on 6/19/2016.
 */
public class RoadStopModel {
    private List<Integer> roadList;
    private Integer number;

    public List<Integer> getRoadList() {
        return roadList;
    }

    public void setRoadList(List<Integer> roadList) {
        this.roadList = roadList;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public RoadStopModel(List<Integer> roadList, Integer number) {
        this.roadList = roadList;
        this.number = number;
    }
}
