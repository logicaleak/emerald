package app.api.model;

import java.util.List;

/**
 * Created by ozum on 6/19/2016.
 */
public class Path {
    private List<Integer> path;
    private Integer cost;

    public List<Integer> getPath() {
        return path;
    }

    public void setPath(List<Integer> path) {
        this.path = path;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
