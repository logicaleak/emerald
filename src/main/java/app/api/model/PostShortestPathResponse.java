package app.api.model;

/**
 * Created by ozum on 6/19/2016.
 */
public class PostShortestPathResponse {
    private Path path;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }
}
