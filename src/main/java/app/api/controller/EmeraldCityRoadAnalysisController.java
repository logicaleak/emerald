package app.api.controller;

import app.algo.exception.IllegalDestinationException;
import app.api.model.PostEmeraldCityRoadRequest;
import app.api.model.PostEmeraldCityRoadResponse;
import app.api.service.EmeraldCityRoadService;
import app.worldofoz.exception.PortalEligibilityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ozum on 6/19/2016.
 */
@RestController
public class EmeraldCityRoadAnalysisController {

    @Autowired
    private EmeraldCityRoadService emeraldCityRoadService;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/cityroad", method = RequestMethod.POST, consumes = "application/json")
    public PostEmeraldCityRoadResponse getEmeraldCityRoadDetails(@RequestBody PostEmeraldCityRoadRequest postEmeraldCityRoadRequest)
            throws PortalEligibilityException, IllegalDestinationException {
        return emeraldCityRoadService.getEmeraldCityRoadDetails(postEmeraldCityRoadRequest);
    }
}
