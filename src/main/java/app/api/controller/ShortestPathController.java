package app.api.controller;

import app.algo.exception.IllegalDestinationException;
import app.api.model.PostShortestPathRequest;
import app.api.model.PostShortestPathResponse;
import app.api.service.EmeraldCityRoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import app.worldofoz.exception.PortalEligibilityException;

/**
 * Created by ozum on 6/19/2016.
 */
@RestController
public class ShortestPathController {

    @Autowired
    private EmeraldCityRoadService emeraldCityRoadService;

    @RequestMapping(value = "/shortestpath", method = RequestMethod.POST)
    public PostShortestPathResponse returnShortestPath(@RequestBody PostShortestPathRequest postShortestPathRequest)
            throws PortalEligibilityException, IllegalDestinationException {

        return emeraldCityRoadService.getShortestPath(postShortestPathRequest);
    }
}
