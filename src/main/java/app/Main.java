package app;

import app.algo.PriorityShortestPathFinder;
import app.graph.DirectedGraph;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by ozum on 6/18/2016.
 */
@SpringBootApplication
public class Main implements CommandLineRunner {
    @Override
    public void run(String... strings) throws Exception {

    }

    public static void main(String[] args) throws Exception {
        DirectedGraph map = MapReader.startRead("/data.dat").read().getGraph();
        PriorityShortestPathFinder priorityShortestPathFinder = new PriorityShortestPathFinder(map);

    }
}
